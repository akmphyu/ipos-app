import 'package:flutter/material.dart';
import 'screens/dashboard_screen.dart';
import 'screens/issue_note_screen.dart';
import 'screens/receipt_note_screen.dart';
import 'screens/product_screen.dart';
import 'screens/vendor_screen.dart';
import 'screens/business_party_user_screen.dart';
import 'screens/create_client.dart';
import 'screens/create_vendor.dart';
import 'screens/products_screen.dart';
import 'screens/products_details_screen.dart';
import 'screens/vendor_detail_screen.dart';
import 'screens/client_detail_screen.dart';
import 'screens/issue_note_tabs_screen.dart';
import 'package:provider/provider.dart';
import 'providers/cart.dart';
import 'screens/receipt_cart_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Cart(),
        ),
      ],
      child: MaterialApp(
        title: 'iPOS',
        theme: ThemeData(
          primarySwatch: Colors.purple,
          accentColor: Colors.deepPurple,
          fontFamily: 'Raleway',
          textTheme: ThemeData.light().textTheme.copyWith(
                body1: TextStyle(
                  color: Color.fromRGBO(20, 51, 51, 1),
                ),
                body2: TextStyle(
                  color: Color.fromRGBO(20, 51, 51, 1),
                ),
                title: TextStyle(
                    fontSize: 24,
                    fontFamily: 'RobotoCondensed',
                    fontWeight: FontWeight.bold),
              ),
        ),
        initialRoute: '/',
        routes: {
          '/': (ctx) => DashboardScreen(),
          IssueNoteScreen.routeName: (ctx) => IssueNoteScreen(),
          IssueNoteTabsScreen.routeName: (ctx) => IssueNoteTabsScreen(),
          ReceiptNoteScreen.routeName: (ctx) => ReceiptNoteScreen(),
          ProductScreen.routeName: (ctx) => ProductScreen(),
          VendorScreen.routeName: (ctx) => VendorScreen(),
          CreateClient.routeName: (ctx) => CreateClient(),
          CreateVendor.routeName: (ctx) => CreateVendor(),
          BusinessPartyUserScreen.routeName: (ctx) => BusinessPartyUserScreen(),
          ProductsScreen.routeName: (ctx) => ProductsScreen(),
          ProductsDetailsScreen.routeName: (ctx) => ProductsDetailsScreen(),
          VendorDetailScreen.routeName: (ctx) => VendorDetailScreen(),
          ClientDetailScreen.routeName: (ctx) => ClientDetailScreen(),
          ReceiptCartScreen.routeName: (ctx) => ReceiptCartScreen()
        },
      ),
    );
  }
}
