import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class CartItem{
  final String productId;
  final int productQty;
  CartItem({this.productId, this.productQty});
}
class Cart with ChangeNotifier {
 List<String> _poItems = [];
 List<String> _doItems = [];

 List<String> get poItems{
   return [..._poItems];
 }
 List<String> get doItems{
   return [..._doItems];
 }
 void addPoItem(String productId) {
   _poItems.add(productId);
   notifyListeners();
 }
 void addDoItem(String productId) {
   _doItems.add(productId);
   notifyListeners();
 }
 int get poItemCount{
   return _poItems.length;
 }
 int get doItemCount{
   return _doItems.length;
 }

 }

