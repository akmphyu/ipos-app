import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../widgets/client.dart';
import 'create_client.dart';
import '../widgets/search.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'client_select_form.dart';


class BusinessPartyUserScreen extends StatefulWidget {
  static const routeName = '/client';

  @override
  _BusinessPartyUserScreenState createState() => _BusinessPartyUserScreenState();
}

class _BusinessPartyUserScreenState extends State<BusinessPartyUserScreen> {
  Map<String, dynamic> businessPartyUsers;
  List<dynamic> businessPartyUserList;

  getData() async{
    http.Response response = await http
        .get(Uri.encodeFull("http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/business-party-user/get-business-party-user"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        }

    );
    this.setState((){
      businessPartyUsers = json.decode(response.body);
      businessPartyUserList = businessPartyUsers['data'];
    });

  }

  @override
  void initState(){
this.getData();
    super.initState();
  }
  createClient(BuildContext context){
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => ClientSelectForm()));
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Client & Address"),
      ),
      drawer: Drawer(
        child: MainDrawer(),
      ),
      body: ListView.builder(itemBuilder: (BuildContext context, int index){
        return Client(id: businessPartyUserList[index]['trx_no'],clientName: businessPartyUserList[index]['business_party_user_name']);
      },itemCount: businessPartyUserList == null ? 0: businessPartyUserList.length,

      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () => createClient(context),
        child: Icon(Icons.add),
      ),
    );
  }
}
