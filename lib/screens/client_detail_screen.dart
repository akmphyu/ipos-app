import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ClientDetailScreen extends StatefulWidget {
  static const routeName = '/client-detail';
  final String clientId;
  ClientDetailScreen({this.clientId});

  @override
  _ClientDetailScreenState createState() => _ClientDetailScreenState();
}

class _ClientDetailScreenState extends State<ClientDetailScreen> {
  Map<String, dynamic> clients;

  List<dynamic> clientList;

  getData(id) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/business-party-user/get-business-party-user-by-id?id=${id}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      clients = json.decode(response.body);
      clientList = clients['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.clientId);
    super.initState();
  }

  Map routeArgs = {};

  @override
  Widget build(BuildContext context) {
     routeArgs = ModalRoute.of(context).settings.arguments;
//      print(routeArgs);


//    setState(() {
//      id = routeArgs['id'];
//    });

    return Scaffold(
      appBar: AppBar(
        title: Text("${clientList[0]['business_party_user_name']}"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.all(5),
          child: Column(
            children: <Widget>[
              Card(
                elevation: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      alignment: Alignment.center,
                      child: CircleAvatar(
                        radius: 30.0,
                        child: Image.asset(
                          'assets/images/avatar2.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Row(

              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                RaisedButton(
                  child: Text('Edit'),
                  color: Colors.yellow,

                  onPressed: (){

                  },
                ),
                RaisedButton(
                  child: Text('Delete'),
                  color: Colors.red,

                  onPressed: (){

                  },
                )
              ],
            ),
          ),
                    Container(
                      child: Text(
                        'Billing Address',
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),
                      ),
                      padding: EdgeInsets.only(top:20, bottom: 10, left: 10),

                    ),
                    ClientText('Address', '${clientList[0]['billing_address']}'),
                    ClientText('Country', '${clientList[0]['billing_country']}'),
                    ClientText(
                        'Postal Code', '${clientList[0]['billing_postal']}'),
                    ClientText(
                        'Contact Name', '${clientList[0]['billing_contact']}'),
                    ClientText(
                        'Telephone', '${clientList[0]['billing_telephone']}'),
                    ClientText('HP', '${clientList[0]['billing_hp']}'),
                    ClientText('Target Sale', '${clientList[0]['target_sale']}'),
                    ClientText('Rebate', '${clientList[0]['rebate']}'),
                    ClientText('Discount', '${clientList[0]['discount']}'),
                  ],
                ),
              ),
              Card(
                elevation: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        'Delivery Address',
                        style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),
                      ),
                      padding: EdgeInsets.only(top:20, bottom: 10, left: 10),

                    ),
                    ClientText('Address', '${clientList[0]['deliery_address']}'),
                    ClientText('Country', '${clientList[0]['delivery_country']}'),
                    ClientText(
                        'Postal Code', '${clientList[0]['delivery_postal']}'),
                    ClientText(
                        'Contact Name', '${clientList[0]['delivery_contact']}'),
                    ClientText(
                        'Telephone', '${clientList[0]['delivery_telephone']}'),
                    ClientText('HP', '${clientList[0]['delivery_hp']}')
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ClientText extends StatelessWidget {
  final String displayName;
  final String displayText;
  ClientText(this.displayName, this.displayText);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              '${displayName}:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(child: Text(displayText))
        ],
      ),
    );
  }
}
