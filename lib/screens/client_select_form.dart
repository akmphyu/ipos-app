import 'package:flutter/material.dart';
import 'create_client.dart';

class ClientSelectForm extends StatefulWidget {
  @override
  _ClientSelectFormState createState() => _ClientSelectFormState();
}

class _ClientSelectFormState extends State<ClientSelectForm> {
  String _selectedClient;



  List clients = [
    {
      'name': 'WMO',
      'id' : '33'
    },
    {
      'name':'AGL',
      'id': '34'
    },
    {
      'name':'AF',
      'id': '37'
    },
    {
      'name':'WMO2',
      'id': '104'
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text("Create New Client")),body: Column(children: [ DropdownButton(
      value: _selectedClient,
      items: clients.map((client) {
        return DropdownMenuItem(
            value: client['id'], child: Text(client['name']));
      }).toList(),
      onChanged: (newValue) {
        setState(() {
          _selectedClient = newValue;
        });
      },
    ),
      RaisedButton(onPressed:(){
        Navigator.of(context).push(MaterialPageRoute(builder: (_) => CreateClient(clientId: _selectedClient,) ));
      },child: Text('Next'),)
    ],),);
  }
}


