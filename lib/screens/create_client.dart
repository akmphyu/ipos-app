import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'business_party_user_screen.dart';
import 'dart:async';
import 'dart:convert';

class CreateClient extends StatefulWidget {
  static const routeName = '/create_client';
  final String clientId;
  CreateClient({this.clientId});

  @override
  _CreateClientState createState() => _CreateClientState();
}

class _CreateClientState extends State<CreateClient> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController clientController = TextEditingController();
  TextEditingController billingAddressController = TextEditingController();
  TextEditingController billingCountryController = TextEditingController();
  TextEditingController billingPostalCodeController = TextEditingController();
  TextEditingController billingContactNameController = TextEditingController();
  TextEditingController billingTelephoneController = TextEditingController();
  TextEditingController billingHpController = TextEditingController();
  TextEditingController deliveryAddressController = TextEditingController();
  TextEditingController deliveryCountryController = TextEditingController();
  TextEditingController deliveryPostalCodeController = TextEditingController();
  TextEditingController deliveryContactNameController = TextEditingController();
  TextEditingController deliveryTelephoneController = TextEditingController();
  TextEditingController deliveryHpController = TextEditingController();
  TextEditingController remarkController = TextEditingController();

  Column FormField(String text, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(text),
        ),
        TextField(
          controller: controller,
        )
      ],
    );
  }
  submitForm(String client, String billingAddress, String billingCountry, String billingPostal, String billingContact, String billingTelephone, String billingHp,String deliveryAddress, String deliveryCountry, String deliveryPostal, String deliveryContact, String deliveryTelephone, String deliveryHp, String remark) async{
    await http
        .post(Uri.encodeFull("http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/business-party-user/create-business-party-user"),
        headers: {

          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        },
        body:{
          "business_party_trx_no": this.widget.clientId,
//          "business_party_user_code": "z"+ DateFormat('yMdHms').format(DateTime.now()),
          "business_party_user_code": "z"+ DateFormat('yMdHms').format(DateTime.now()),
          "business_party_user_name": client,
          "address_line1": billingAddress,
          "country_code": billingCountry,
          "postal_code": billingPostal,
          "contact": billingContact,
          "telephone": billingTelephone,
          "hp": billingHp,
          "outlet_address_line1": deliveryAddress,
          "outlet_country_code": deliveryCountry,
          "outlet_postal_code": deliveryPostal,
          "outlet_contact": deliveryContact,
          "outlet_telephone": deliveryTelephone,
          "outlet_hp": deliveryHp,
          "remarks": remark,
          "created_by":"1",
          "updated_by":"1"

        }

    );
    print("z"+ DateFormat('yMdHms').format(DateTime.now()));
    print(client);
    print(billingAddress);
    print(billingCountry);
    print(billingPostal);
    print(billingContact);
    print(billingTelephone);
    print(billingHp);
    print(deliveryAddress);
    print(deliveryCountry);
    print(deliveryPostal);
    print(deliveryContact);
    print(deliveryTelephone);
    print(deliveryHp);
    print(remark);

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text("Create Client"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                FormField("ClientName", clientController),
                Container(
                  padding: EdgeInsets.only(top: 13.0,bottom: 10.0),
                  alignment: Alignment.topLeft,
                  child: Text("Billing Address",style: TextStyle(fontWeight: FontWeight.bold),),
                ),
                FormField("Address", billingAddressController),
                FormField("Country", billingCountryController),
                FormField("Postal Code", billingPostalCodeController),
                FormField("Contact Name", billingContactNameController),
                FormField("Telephone", billingTelephoneController),
                FormField("HP", billingHpController),
                Container(
                  padding: EdgeInsets.only(top: 13.0,bottom: 10.0),
                  alignment: Alignment.topLeft,
                  child: Text("Delivery Address",style: TextStyle(fontWeight: FontWeight.bold),),
                ),
                FormField("Address", deliveryAddressController),
                FormField("Country", deliveryCountryController),
                FormField("Postal Code", deliveryPostalCodeController),
                FormField("Contact Name", deliveryContactNameController),
                FormField("Telephone", deliveryTelephoneController),
                FormField("HP", deliveryHpController),
                FormField("Remark", remarkController),
                SizedBox(
                  height: 10,
                ),
                RaisedButton(
                  onPressed: (){
                    submitForm(clientController.text, billingAddressController.text, billingCountryController.text, billingPostalCodeController.text, billingCountryController.text, billingTelephoneController.text, billingHpController.text, deliveryAddressController.text, deliveryCountryController.text, deliveryPostalCodeController.text, deliveryContactNameController.text, deliveryTelephoneController.text, deliveryHpController.text, remarkController.text);
                    Navigator.of(context).push(MaterialPageRoute(builder: (_) => BusinessPartyUserScreen()));
                  },
                  color: Colors.blue,
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
