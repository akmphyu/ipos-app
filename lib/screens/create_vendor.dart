import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'vendor_screen.dart';
import 'dart:async';
import 'dart:convert';

class CreateVendor extends StatefulWidget {
  static const routeName = '/create_vendor';

  @override
  _CreateVendorState createState() => _CreateVendorState();
}

class _CreateVendorState extends State<CreateVendor> {
  String _selectedClient;



  List clients = [
    {
      'name': 'WMO',
      'id' : '33'
    },
    {
      'name':'AGL',
      'id': '34'
    },
    {
      'name':'AF',
      'id': '37'
    },
    {
      'name':'WMO2',
      'id': '104'
    }
  ];

  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController countryController = TextEditingController();
  TextEditingController postalCodeController = TextEditingController();
  TextEditingController phoneNumController = TextEditingController();
  TextEditingController contactPersonController = TextEditingController();



  Column FormField(String text, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(
            text,
            style: TextStyle(color: Colors.black),
          ),
        ),
        TextField(controller: controller,),
      ],
    );
  }
  submitForm(String name, String address, String country, String postal, String phone, String contact, String customer) async{
     await http
        .post(Uri.encodeFull("http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/vendor/create-vendor"),
        headers: {

          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        },
       body:{
         "name": name,
         "address": address,
         "country": country,
         "postal_code": postal,
         "phone_numer": phone,
         "contact_person": contact,
         "business_party_trx_no": customer,
       }

    );
     print(name);
     print(address);
     print(country);
     print(postal);
     print(phone);
     print(contact);
     print(customer);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text('Create Vendor'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                FormField("Name", nameController),
                FormField("Address",addressController),
                FormField("Country",countryController),
                FormField("PostalCode",postalCodeController),
                FormField("Phone Number",phoneNumController),
                FormField("Contact Person",contactPersonController),

                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(  padding: EdgeInsets.only(top: 12.0), child: Text('Customer'),),
                    DropdownButton(
                      value: _selectedClient,
                      items: clients.map((client) {
                        return DropdownMenuItem(
                            value: client['id'], child: Text(client['name']));
                      }).toList(),
                      onChanged: (newValue) {
                        setState(() {
                          _selectedClient = newValue;
                        });
                      },
                    ),
                  ],
                ),

//                FormField("Image File", imageController),
                SizedBox(height: 10,),
                RaisedButton(
                  onPressed: (){
                    submitForm(nameController.text, addressController.text, countryController.text, postalCodeController.text, phoneNumController.text, contactPersonController.text, _selectedClient);
                    Navigator.of(context).push(MaterialPageRoute(builder: (_)=> VendorScreen()));
                    },
                  child: Text("Submit", style: TextStyle(color: Colors.white),),
                  color: Colors.blue,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
