import 'package:flutter/material.dart';
import 'issue_note_screen.dart';
import 'receipt_note_screen.dart';
import 'product_screen.dart';
import 'vendor_screen.dart';
import '../widgets/main_drawer.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'business_party_user_screen.dart';

class DashboardScreen extends StatefulWidget {
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isAuth = false;

  Scaffold authScreen() {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dashboard"),
      ),
      drawer: Drawer(
        child: MainDrawer(),
      ),
    );
  }

  Scaffold UnauthScreen() {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [Colors.blue, Colors.teal],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[textSection(), buttonSection()],
        ),
      ),
    );
  }

  logIn(String username, String password) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/v2/user/get-user?username=${username}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        });
    Map<String, dynamic> users = json.decode(response.body);
    List<dynamic> user = users['data'];
    if (user[0]['username'] == username.toUpperCase()) {
      if(user[0]['password'] == password) {
        this.setState(() {
          isAuth = true;
        });
      }else{
        this.setState(() {
          isAuth = false;
        });
      }
    } else {
      this.setState(() {
        isAuth = false;
      });
    }
  }

  Container buttonSection() {
    return Container(
      width: (MediaQuery.of(context).size.width - 50),
      height: 40.0,
      margin: EdgeInsets.only(top: 30.0),
      child: RaisedButton(
        onPressed: () =>
            logIn(usernameController.text, passwordController.text),
        color: Theme.of(context).accentColor,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        child: Text(
          "Sign In",
          style: TextStyle(color: Colors.white70),
        ),
      ),
    );
  }

  Container textSection() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      margin: EdgeInsets.only(top: 30.0),
      child: Column(
        children: <Widget>[
          txtSection("Username", Icons.people, usernameController),
          SizedBox(height: 30.0),
          txtSection("Password", Icons.lock, passwordController)
        ],
      ),
    );
  }

  TextFormField txtSection(
      String title, IconData icon, TextEditingController txtController) {
    return TextFormField(
      controller: txtController,
      style: TextStyle(color: Colors.white70),
      decoration: InputDecoration(
          hintText: title,
          hintStyle: TextStyle(color: Colors.white),
          icon: Icon(icon)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return isAuth ? authScreen() : UnauthScreen();
  }
}
