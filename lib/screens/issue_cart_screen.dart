import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/cart.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'receipt_note_po_form.dart';
import 'issue_note_do_local_form.dart';
import 'issue_note_do_vessel_form.dart';

class IssueCartScreen extends StatefulWidget {
  static const routeName = '/cart-screen';
  final List<String> cartList;
  final String client;
  final String warehouse;
  final String doType;

  IssueCartScreen({this.cartList, this.client, this.warehouse,this.doType});

  @override
  _IssueCartScreenState createState() => _IssueCartScreenState();
}

class _IssueCartScreenState extends State<IssueCartScreen> {
  Map<String, dynamic> carts;
  List<dynamic> cartsData;
  List<dynamic> cartListData = [];



  getData(cartIdList) async {
//    cartIdList.map((cartId) async {
    for (String i in cartIdList) {
      http.Response response = await http.get(
          Uri.encodeFull(
              "http://uat.wms.winspecgroup.com/wms/web/v2/product/get-product-by-name?id=${i}"),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
          });
      this.setState(() {
        carts = json.decode(response.body);
        cartsData = carts['msg'];

        if (cartsData != null) {
          cartListData.add(cartsData);
        }
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState

   this.getData(widget.cartList);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Cart'),
      ),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemBuilder: (ctx, index) => CartProductItem(
                productId: cartListData[index][0]['trx_no'],
                productCode: cartListData[index][0]['product_code'],
                productDescription: cartListData[index][0]['description'],
              ),
              itemCount: cartListData.length,
            ),
          ),
          RaisedButton(
            onPressed: () {
             widget.doType == 'local' ? Navigator.of(context)
                  .push(MaterialPageRoute(builder: (_) => IssueNoteDoLocalForm(client: widget.client, warehouse: widget.warehouse,))) :  Navigator.of(context)
                 .push(MaterialPageRoute(builder: (_) => IssueNoteDoVesselForm(client: widget.client, warehouse: widget.warehouse,)));
            },
            child: Text('Next', style: TextStyle(color: Colors.white)),
            color: Colors.blue,
          )
        ],
      ),
    );
  }
}

class CartProductItem extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  TextEditingController qtyController = TextEditingController();

  final String productId;
  final String productCode;
  final String productDescription;
  CartProductItem({this.productId, this.productCode, this.productDescription});
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
      child: Padding(
        padding: EdgeInsets.all(8),
        child: ListTile(
          leading: CircleAvatar(
            backgroundImage: AssetImage('assets/images/avatar2.png'),
          ),
          title: Text(productCode),
          subtitle: Text(productDescription),
          trailing: Container(
            width: 40,
            child: Form(
              key: _formKey,
              child: TextField(
                controller: qtyController,
                keyboardType: TextInputType.number,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
