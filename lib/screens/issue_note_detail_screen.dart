import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class IssueNoteDetailScreen extends StatefulWidget {
  final String issue_note_id;
  IssueNoteDetailScreen({this.issue_note_id});

  @override
  _IssueNoteDetailScreenState createState() => _IssueNoteDetailScreenState();
}

class _IssueNoteDetailScreenState extends State<IssueNoteDetailScreen> {
  Map<String, dynamic> issue_note;
  Map<String, dynamic>  issueNoteList;
  List<dynamic> productList;

  getData(id) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/issue-note/retrieve-gi?issue_note_id=${id}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      issue_note = json.decode(response.body);
      issueNoteList = issue_note['msg'];
      productList = issueNoteList['Product_list'];

    });
//  print(productList[0]['product_code']);
  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.issue_note_id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
        title: Text('${issueNoteList['GI_Detail']['ref_no']}'),
    ),
      body:Card(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(

                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RaisedButton(
                          child: Text('Edit'),
                          color: Colors.yellow,

                          onPressed: (){

                          },
                        ),
                        RaisedButton(
                          child: Text('Delete'),
                          color: Colors.red,

                          onPressed: (){

                          },
                        )
                      ],
                    ),
                  ),
                  CellItem('Sub-Customer:','${issueNoteList['GI_Detail']['SubCustomer']}'),
                  CellItem('Order Date:','${issueNoteList['GI_Detail']['OrderDate']}'),
                  CellItem('Delivery Date:','${issueNoteList['GI_Detail']['DeliveryDate']}'),
                  CellItem('Delivery Time:','${issueNoteList['GI_Detail']['DeliveryTime']}'),
                  CellItem('Client:','${issueNoteList['business_party_code']}'),


                  CellItem('Local/Vessel:','${issueNoteList['GI_Detail']['is_local'] == 1 ? "Local" : "Vessel"}'),
                  CellItem('Status:','${issueNoteList['GI_Detail']['status']}'),
                  CellItem('Remark:','${issueNoteList['GI_Detail']['JobRemarks']}'),

                ],
              ),
            ),
            Divider(),
            Expanded(
              child: SizedBox(
                height: 200.0,
                child: ListView.builder(itemBuilder: (BuildContext context, int index){
                  return ProductDetailItem(prodCode: productList[index]['product_code'],prodDes: productList[index]['description'],qty: productList[index]['qty'],uom: productList[index]['uom'],);
                },itemCount: productList == null ? 0: productList.length,

                ),
              ),
            ),

          ],
        ),
      ),
    )
    );
  }
}

class CellItem extends StatelessWidget {
  final String title;
  final String value;
  CellItem(this.title, this.value);

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(child: Text('${title}', style: TextStyle(fontWeight: FontWeight.bold),)),
          Expanded(child: Text('${value}')),
        ],
      ),
    );
  }
}

class ProductDetailItem extends StatelessWidget {
  final String prodCode;
  final String prodDes;
  final String qty;
  final String uom;
  ProductDetailItem({this.prodCode,this.prodDes,this.qty,this.uom});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      color: Colors.white70,
      child: Row(
        children: <Widget>[
          Container(
            width: 60,
            height: 80,
            child: Image.asset('assets/images/product_default.jpg'),
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text('Code:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('${prodCode}'),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text('Description:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('${prodDes}'),
                  ],
                ),

                Row(
                  children: <Widget>[
                    Text('Qty:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('${qty}'),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text('UOM:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('${uom}'),

                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

