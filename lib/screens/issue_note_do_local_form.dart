import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class IssueNoteDoLocalForm extends StatefulWidget {
  final String client;
  final String warehouse;

  IssueNoteDoLocalForm({this.client, this.warehouse});

  @override
  _IssueNoteDoLocalFormState createState() => _IssueNoteDoLocalFormState();
}

class _IssueNoteDoLocalFormState extends State<IssueNoteDoLocalForm> {

  String _customerValue;
  Map<String, dynamic> customers;
  List<dynamic> customerList;

  final _formKey = GlobalKey<FormState>();
  TextEditingController doController = TextEditingController();
  TextEditingController orderByController = TextEditingController();
  TextEditingController orderDateController = TextEditingController();
  TextEditingController remarkController = TextEditingController();
  TextEditingController deliveryDateController = TextEditingController();
  TextEditingController deliveryTimeController = TextEditingController();

  Column FormField(String text, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(text),
        ),
        TextField(
          controller: controller,
        )
      ],
    );
  }
  getCustomer(client) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/business-party-user/get-business-party-user-by-client?client=${client}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      customers = json.decode(response.body);
      customerList = customers['data'];
    });
  }
  @override
  void initState() {
    // TODO: implement initState

    this.getCustomer(33);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Create Local DO Form'),),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                FormField("DO No.*",doController),
                FormField("Order By",orderByController),
                FormField("Order Date",orderDateController),
                FormField("Remarks",remarkController),
                SizedBox(height: 20.0,),
                Container(

                    child: DropdownButton(
                      value: _customerValue,
                      items: customerList.map((customer) {
                        return

                          DropdownMenuItem(child: FittedBox(child: Text(customer['business_party_user_name'])), value: customer['trx_no'],);

                      }).toList(),
                      hint: Text('Select Sub-Customer'),
                      onChanged: (value){
                        setState((){
                          _customerValue = value;
                        });
                      },
                    )
                ),
                FormField("Delivery Date",deliveryDateController),
                FormField("Delivery Time",deliveryTimeController),
                RaisedButton(child: Text('Submit',style: TextStyle(color: Colors.white),),onPressed: (){},color: Colors.blue,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
