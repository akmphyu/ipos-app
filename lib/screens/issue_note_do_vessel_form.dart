import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class IssueNoteDoVesselForm extends StatefulWidget {
  final String client;
  final String warehouse;

  IssueNoteDoVesselForm({this.client, this.warehouse});
  @override
  _IssueNoteDoVesselFormState createState() => _IssueNoteDoVesselFormState();
}

class _IssueNoteDoVesselFormState extends State<IssueNoteDoVesselForm> {
  Map<String, dynamic> customers;
  List<dynamic> customerList;

  Map<String, dynamic> callSigns;
  List<dynamic> callSignList;

  Map<String, dynamic> transporters;
  List<dynamic> transporterList;

  Map<String, dynamic> terminals;
  List<dynamic> terminalList;

  String _customerValue;
  String _callSignValue;
  String _terminalValue;
  String _transporterValue;

  getCustomer(client) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/business-party-user/get-business-party-user-by-client?client=${client}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      customers = json.decode(response.body);
      customerList = customers['data'];
    });
  }

  getCallSign() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/call-sign/get-call-sign"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      callSigns = json.decode(response.body);
      callSignList = callSigns['data'];
    });
  }
  getTransporter() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/transporter/get-transporter"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      transporters = json.decode(response.body);
      transporterList = transporters['data'];
    });
  }
  getTerminal() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/terminal/get-terminal"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      terminals = json.decode(response.body);
      terminalList = terminals['data'];
    });
  }
  @override
  void initState() {
    // TODO: implement initState

    this.getCustomer(33);
    this.getCallSign();
    this.getTerminal();
    this.getTransporter();
    super.initState();
  }

  final _formKey = GlobalKey<FormState>();
  TextEditingController doController = TextEditingController();
  TextEditingController orderByController = TextEditingController();
  TextEditingController orderDateController = TextEditingController();
  TextEditingController remarkController = TextEditingController();
  TextEditingController deliveryDateController = TextEditingController();
  TextEditingController deliveryTimeController = TextEditingController();
  TextEditingController vesselController = TextEditingController();
  TextEditingController imoController = TextEditingController();
  TextEditingController transporterController = TextEditingController();
  TextEditingController contactPersonController = TextEditingController();
  TextEditingController contactNumController = TextEditingController();

  Column FormField(String text, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(text),
        ),
        TextField(
          controller: controller,
        )
      ],
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Create Vessel DO Form'),),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                FormField("DO No.*",doController),
                FormField("Order By",orderByController),
                FormField("Order Date",orderDateController),
                FormField("Remarks",remarkController),
                FormField("Delivery Date",deliveryDateController),
                FormField("Delivery Time",deliveryTimeController),
                Text('Contact Info'),
              Container(
                padding: EdgeInsets.all(20.0),
                child: DropdownButton(
                  value: _customerValue,
                  items: customerList.map((customer) {
                    return

                      DropdownMenuItem(child: FittedBox(child: Text(customer['business_party_user_name'])), value: customer['trx_no'],);

                  }).toList(),
                  hint: Text('Select Sub-Customer'),
                  onChanged: (value){
                    setState((){
                      _customerValue = value;
                    });
                  },
                ),
              ),
              Container(
                  padding: EdgeInsets.all(20.0),
                  width: double.infinity,
                  child: DropdownButton(
                    value: _terminalValue,
                    items: terminalList.map((terminal) {
                      return

                        DropdownMenuItem(child: FittedBox(child: Text(terminal['terminal_code'])), value: terminal['trx_no'],);

                    }).toList(),
                    hint: Text('Select Delivery Port'),
                    onChanged: (value){
                      setState((){
                        _terminalValue = value;
                      });
                    },
                  )
              ),
              Container(
                  padding: EdgeInsets.all(20.0),
                  width: double.infinity,
                  child: DropdownButton(
                    value: _callSignValue,
                    items: callSignList.map((callSign) {
                      return

                        DropdownMenuItem(child: FittedBox(child: Text(callSign['call_sign_name'])), value: callSign['trx_no'],);

                    }).toList(),
                    hint: Text('Select Call Sign'),
                    onChanged: (value){
                      setState((){
                        _callSignValue = value;
                      });
                    },
                  )
              ),
              Container(
                  padding: EdgeInsets.all(20.0),
                  width: double.infinity,
                  child: DropdownButton(
                    value: _transporterValue,
                    items: transporterList.map((transporter) {
                      return

                        DropdownMenuItem(child: FittedBox(child: Text(transporter['name'])), value: transporter['trx_no'],);

                    }).toList(),
                    hint: Text('Select Agent'),
                    onChanged: (value){
                      setState((){
                        _transporterValue = value;
                      });
                    },
                  )
              ),
                FormField("Vessek",vesselController),
                FormField("IMO",imoController),
                FormField("Transporter",transporterController),
                FormField("Contact Person",contactPersonController),
                FormField("Contact Number",contactNumController),

                RaisedButton(child: Text('Submit',style: TextStyle(color: Colors.white),),onPressed: (){},color: Colors.blue,)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
