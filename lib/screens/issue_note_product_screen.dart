import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../widgets/badge.dart';
import 'package:provider/provider.dart';
import '../providers/cart.dart';
import 'issue_cart_screen.dart';

class IssueNoteProductScreen extends StatefulWidget {
  final String client;
  final String warehouse;
  final String doType;
  IssueNoteProductScreen({this.client, this.warehouse, this.doType});

  @override
  _IssueNoteProductScreenState createState() => _IssueNoteProductScreenState();
}

class _IssueNoteProductScreenState extends State<IssueNoteProductScreen> {
  Map<String, dynamic> products;
  List<dynamic> productList;

  getData(client) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.wms.winspecgroup.com/wms/web/v2/product/get-product?business_party_code=${client}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization':
              'Bearer YmVhcmVyIGpqaHNqZ2hram9zcGdmc2hpeUFHREZJSE1TS0JOSkJTRFZQQUtPRVI1NDg1'
        });
    this.setState(() {
      products = json.decode(response.body);
      productList = products['msg'];
    });
  }

//  getData(){
//    print('here');
//  }
  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.client);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    final carts = Provider.of<Cart>(context).doItems;
    return Scaffold(
      appBar: AppBar(title: Text('Products'), actions: <Widget>[
        Consumer<Cart>(
          builder: (_, cart, ch) => Badge(
            child: ch,
            value: cart.doItemCount.toString(),
          ),
          child: IconButton(
            icon: Icon(Icons.shopping_cart),
            color: Colors.white,
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (_) => IssueCartScreen(cartList: carts, client:widget.client,warehouse: widget.warehouse,doType: widget.doType,)));
            },
          ),
        ),
      ]),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GridView.builder(
          itemBuilder: (BuildContext context, int index) {
            return ProductGridItem(
              productList[index]['trx_no'],
              productList[index]['product_code'],
              productList[index]['description'],
            );
          },
          itemCount: productList == null ? 0 : productList.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
          ),
        ),
      ),
    );
  }
}

class ProductGridItem extends StatelessWidget {
  final String productId;
  final String productCode;
  final String productDescription;

  ProductGridItem(this.productId, this.productCode, this.productDescription);
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Cart>(context, listen: false);
    return GestureDetector(
      onTap: () => print('product added'),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: GridTile(
          child: Image(
            image: AssetImage('assets/images/product_default.jpg'),
            fit: BoxFit.cover,
          ),
          footer: GridTileBar(
            backgroundColor: Colors.black87,
            title: FittedBox(
              child: Text(
                productCode,
              ),
            ),
            subtitle: FittedBox(
              child: Text(
                productDescription,
              ),
            ),
            trailing: IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
                product.addDoItem(productId);
              },
            ),
          ),
        ),
      ),
    );
  }
}
