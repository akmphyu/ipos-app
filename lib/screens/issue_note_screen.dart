import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../widgets/issue_note_item.dart';
import '../widgets/search.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'issue_note_select_form.dart';

class IssueNoteScreen extends StatefulWidget {
  static const routeName = '/issue_note';
  final String jobStatus;
  IssueNoteScreen({this.jobStatus = "all"});

  @override
  _IssueNoteScreenState createState() => _IssueNoteScreenState();
}

class _IssueNoteScreenState extends State<IssueNoteScreen> {
  Map<String, dynamic> issue_note;
  List<dynamic> issueNoteList;




  getData(jobStatus) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/issue-note/get-do-list?business_party_code=WMO&dateFrom=2020-01-02&dateTo=2020-12-16&jobStatus=${jobStatus}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      issue_note = json.decode(response.body);
      issueNoteList = issue_note['msg'];
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.jobStatus);

    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return IssueNoteItem(
            issue_note_id: issueNoteList[index]['trx_no'],
            ref_no: issueNoteList[index]['ref_no'],
            order_date: issueNoteList[index]['OrderDate'],
            delivery_date: issueNoteList[index]['DeliveryDate'],
            status: issueNoteList[index]['status'],
            is_local: issueNoteList[index]['is_local'],
            client: issueNoteList[index]['business_party_code'],
            remark: issueNoteList[index]['JobRemarks'],
          );
        },
        itemCount: issueNoteList == null ? 0 : issueNoteList.length,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => IssueNoteSelectForm() ));
        },
      ),
    );
  }
}
