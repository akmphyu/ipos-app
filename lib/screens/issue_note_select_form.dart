import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'issue_note_product_screen.dart';


class IssueNoteSelectForm extends StatefulWidget {
  @override
  _IssueNoteSelectFormState createState() => _IssueNoteSelectFormState();
}

class _IssueNoteSelectFormState extends State<IssueNoteSelectForm> {
  String _selectedClient;
  String _selectedWarehouse;
  String _selectedDoType;


  Map<String, dynamic> warehouses;
  List<dynamic> warehouseList;

  List clients = [
    {
      'name': 'WMO',
      'id' : '33'
    },
    {
      'name':'AGL',
      'id': '34'
    },
    {
      'name':'AF',
      'id': '37'
    },
    {
      'name':'WMO2',
      'id': '104'
    }
  ];
  List doTypes = [
    {
      'name':"Create Local Do",
      'id':'local'
    },
    {
      'name':'Create Vessel Do',

    }
  ];

  getWarehouse() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/warehouse/get-warehouse"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      warehouses = json.decode(response.body);
      warehouseList = warehouses['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState

    this.getWarehouse();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('DO Form'),),
      body: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            DropdownButton(
              value: _selectedClient,
              items: clients.map((client) {
                return DropdownMenuItem(
                    value: client['id'], child: Text(client['name']));
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  _selectedClient = newValue;
                });
              },
            ),
            DropdownButton(
              value: _selectedWarehouse,
              items: warehouseList.map((warehouse) {
                return

                  DropdownMenuItem(child: Text(warehouse['warehouse_code']), value: warehouse['trx_no'],);

              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  _selectedWarehouse = newValue;
                });
              },
            ),

            DropdownButton(
              value: _selectedDoType,
              items: doTypes.map((doType) {
                return DropdownMenuItem(
                    value: doType['id'], child: Text(doType['name']));
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  _selectedDoType = newValue;
                });
              },
            ),
            RaisedButton(onPressed:(){
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => IssueNoteProductScreen(warehouse: _selectedWarehouse,client: _selectedClient,doType: _selectedDoType,) ));
            },child: Text('Next'),)
          ],
        ),
      ),

    );
  }
}
