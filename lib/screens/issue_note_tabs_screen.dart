import 'package:flutter/material.dart';
import './issue_note_screen.dart';
import '../widgets/main_drawer.dart';

class IssueNoteTabsScreen extends StatefulWidget {
  static const routeName = '/issue_note_tabs_screen';
  @override
  _IssueNoteTabsScreenState createState() => _IssueNoteTabsScreenState();
}

class _IssueNoteTabsScreenState extends State<IssueNoteTabsScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: Text('DO'),

          bottom: TabBar(

            tabs: [
              FittedBox(
                child: Tab(

                  text: 'All',
                ),
              ),
              FittedBox(child: Tab(text: 'Pending Picking')),
              FittedBox(
                child: Tab(
                  text: 'Released',
                ),
              ),
              FittedBox(
                child: Tab(
                  text: 'Delivered',
                ),
              )
            ],
          ),
        ),
        drawer: Drawer(
          child: MainDrawer(),
        ),
        body: TabBarView(children: [
          IssueNoteScreen(jobStatus: 'all',),
          IssueNoteScreen(jobStatus: 'pending_picking',),
          IssueNoteScreen(jobStatus: 'released',),
          IssueNoteScreen(jobStatus: 'delivered',),
        ],),
      ),
    );
  }
}
