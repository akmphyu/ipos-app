import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import '../widgets/product_client_item.dart';

class ProductScreen extends StatelessWidget {

  static const routeName = '/product';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("iPOS"),
      ),
      drawer: Drawer(
        child: MainDrawer(),
      ),
      body: GridView(
        padding: const EdgeInsets.all(25),
        children: <Widget>[
          ProductItem('m1', 'WMO', Colors.purple),
          ProductItem('m3', 'AGL', Colors.red),
          ProductItem('m4', 'AF', Colors.green),
          ProductItem('m2', 'WMO2', Colors.orange),
        ],
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
      ),
    );
  }
}
