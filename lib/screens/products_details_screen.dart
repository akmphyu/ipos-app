import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ProductsDetailsScreen extends StatefulWidget {
  static const routeName = '/products-details';
  final String productId;
  ProductsDetailsScreen({this.productId});

  @override
  _ProductsDetailsScreenState createState() => _ProductsDetailsScreenState();
}

class _ProductsDetailsScreenState extends State<ProductsDetailsScreen> {

  Map<String, dynamic> products;
  List<dynamic> productList;

  getData(id) async{
    http.Response response = await http
        .get(Uri.encodeFull("http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/product/get-product-by-name?id=${id}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        }

    );
    this.setState((){
      products = json.decode(response.body);
      productList = products['data'];
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.productId);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('${productList[0]['product_code']}'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 15.0),
                width: double.infinity,
                height: 200.0,
                child: Image.asset(
                  'assets/images/product_default.jpg',
                  fit: BoxFit.cover,
                ),
              ),
              SizedBox(height: 10.0,),

             ProductText("Client Name","${productList[0]['business_party_abbr_name']}"),
              Divider(),
              ProductText("Description","${productList[0]['description']}"),
              Divider(),
              ProductText("Product Group","${productList[0]['product_group_code']}"),
              Divider(),
              ProductText("Product Class","${productList[0]['product_class_name']}"),
              Divider(),
              ProductText("Brand Name","${productList[0]['brand_naem']}"),
              Divider(),
              ProductText("HS Code","${productList[0]['hs_code']}"),
              Divider(),
              ProductText("Max Qty", "${productList[0]['max_qty']}"),
              Divider(),
              ProductText("Min Qty","${productList[0]['min_qty']}"),
              Divider(),
              ProductText("Weight(Kg)","${productList[0]['base_uom_weight_kg']}"),
              Divider(),
              ProductText("Length(M)","${productList[0]['base_uom_length_meter']}"),
              Divider(),
              ProductText("Width(M)","${productList[0]['base_uom_width_meter']}"),
              Divider(),
              ProductText("Height(M)","${productList[0]['base_uom_height_meter']}"),
              Divider(),
              ProductText("UOM","${productList[0]['billing_uom_rate_to_base_uom']}"),
              Divider(),
              ProductText("Qty per Pallet/Box","need to add"),
              Divider(),
              ProductText("Qty per Carton","${productList[0]['qty_per_carton']}"),
              Divider(),
              ProductText("Pallet Stackable","need to add"),
              Divider(),
              ProductText("Issue Method","${productList[0]['issue_method_code']}"),
              Divider(),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RaisedButton(
                      child: Text('Edit'),
                      color: Colors.yellow,

                      onPressed: (){

                      },
                    ),
                    RaisedButton(
                      child: Text('Delete'),
                      color: Colors.red,

                      onPressed: (){

                      },
                    )
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}



class ProductText extends StatelessWidget {
  final String displayName;
  final String displayText;
  ProductText(this.displayName, this.displayText);
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
       mainAxisAlignment: MainAxisAlignment.spaceEvenly,

        children: <Widget>[
          Expanded(child: Text("${displayName}:",style: TextStyle(fontWeight: FontWeight.bold),)),
          Expanded(child: Text(displayText)),
        ],
      ),
    );
  }
}

