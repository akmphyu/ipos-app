import 'dart:io';

import 'package:flutter/material.dart';
import '../widgets/search.dart';
import 'products_details_screen.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ProductsScreen extends StatefulWidget {
  static const routeName = '/products';

  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {


  Map<String, dynamic> products;
  List<dynamic> productList;

  getData() async{
  http.Response response = await http
      .get(Uri.encodeFull("http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/product/get-product"),
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
  }

  );
  this.setState((){
    products = json.decode(response.body);
    productList = products['data'];
  });

}
//  getData(){
//    print('here');
//  }
  @override
  void initState() {
    // TODO: implement initState
    this.getData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    print(productList);
    final routeArgs = ModalRoute.of(context).settings.arguments as Map<String, String>;
    return Scaffold(
      appBar: AppBar(
        title: Text('Products'),
      ),
      body:
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GridView.builder(itemBuilder: (BuildContext context, int index){
              return ProductGridItem(productList[index]['trx_no'],productList[index]['product_code'],productList[index]['description']);
            },
              itemCount: productList == null? 0: productList.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
              ),
            ),
          ),

    );
  }
}

class ProductGridItem extends StatelessWidget {
  final String productId;
  final String productCode;
  final String productDescription;
  ProductGridItem(this.productId, this.productCode,this.productDescription);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>  Navigator.of(context).push(MaterialPageRoute(builder: (_) => ProductsDetailsScreen(productId: productId,))),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: GridTile(
          child: Image(
            image: AssetImage('assets/images/product_default.jpg'),
            fit: BoxFit.cover,
          ),
          footer: GridTileBar(
            backgroundColor: Colors.black87,
            title: FittedBox(child: Text(productCode,),),
            subtitle: FittedBox(child: Text(productDescription,),),

          ),
        ),
      ),
    );
  }
}
