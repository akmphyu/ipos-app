import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ReceiptNoteDetailScreen extends StatefulWidget {
  final String receipt_note_id;
  ReceiptNoteDetailScreen({this.receipt_note_id});

  @override
  _ReceiptNoteDetailScreenState createState() => _ReceiptNoteDetailScreenState();
}

class _ReceiptNoteDetailScreenState extends State<ReceiptNoteDetailScreen> {
  Map<String, dynamic> receipt_note;
  Map<String, dynamic>  receiptNoteList;
  List<dynamic> productList;

  getData(id) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/receipt-note/retrieve-gr?receipt_note_id=${id}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      receipt_note = json.decode(response.body);
      receiptNoteList = receipt_note['msg'];
      productList = receiptNoteList['Product_list'];

    });
//  print(receiptNoteList);
  }

  @override
  void initState() {
    // TODO: implement initState

    this.getData(widget.receipt_note_id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text('${receiptNoteList['GR_Detail']['ref_no']}'),
        ),
        body:Card(
          child: Container(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(

                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RaisedButton(
                              child: Text('Edit'),
                              color: Colors.yellow,

                              onPressed: (){

                              },
                            ),
                            RaisedButton(
                              child: Text('Delete'),
                              color: Colors.red,

                              onPressed: (){

                              },
                            )
                          ],
                        ),
                      ),
                      CellItem('Sub-Customer:','${receiptNoteList['GR_Detail']['SubCustomer']}'),
                      CellItem('Order Date:','${receiptNoteList['GR_Detail']['OrderDate']}'),
                      CellItem('Vendor:','${receiptNoteList['GR_Detail']['vendor']}'),
                      CellItem('Client:','${receiptNoteList['business_party_code']}'),
                      CellItem('Warehouse:','${receiptNoteList['GR_Detail']['warehouse']}'),

                      CellItem('Cross-docking:','${receiptNoteList['GR_Detail']['is_local'] == 1 ? "Yes" : "No"}'),
                      CellItem('Status:','${receiptNoteList['GR_Detail']['status']}'),
                      CellItem('Remark:','${receiptNoteList['GR_Detail']['JobRemarks']}'),

                    ],
                  ),
                ),
                Divider(),
                Expanded(
                  child: SizedBox(
                    height: 200.0,
                    child: ListView.builder(shrinkWrap:true,itemBuilder: (BuildContext context, int index){
                      return ProductDetailItem(prodCode: productList[index]['product_code'],prodDes: productList[index]['description'],qty: productList[index]['qty'],uom: productList[index]['uom'],);
                    },itemCount: productList == null ? 0: productList.length,

                    ),
                  ),
                ),

              ],
            ),
          ),
        )
    );
  }
}

class CellItem extends StatelessWidget {
  final String title;
  final String value;
  CellItem(this.title, this.value);

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(child: Text('${title}', style: TextStyle(fontWeight: FontWeight.bold),)),
          Expanded(child: Text('${value}')),
        ],
      ),
    );
  }
}

class ProductDetailItem extends StatelessWidget {
  final String prodCode;
  final String prodDes;
  final String qty;
  final String uom;
  ProductDetailItem({this.prodCode,this.prodDes,this.qty,this.uom});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      width: double.infinity,
      color: Colors.white70,
      child: Row(
        children: <Widget>[
          Container(
            width: 60,
            height: 80,
            child: Image.asset('assets/images/product_default.jpg'),
          ),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text('Code:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('${prodCode}'),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text('Description:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Container(child: Text('${prodDes}')),
                  ],
                ),

                Row(
                  children: <Widget>[
                    Text('Qty:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('${qty}'),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text('UOM:', style: TextStyle(fontWeight: FontWeight.bold),),
                    SizedBox(width: 10,),
                    Text('${uom}'),

                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

