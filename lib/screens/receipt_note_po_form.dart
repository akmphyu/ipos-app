import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ReceiptNotePoForm extends StatefulWidget {
  final String clientId;
  ReceiptNotePoForm({this.clientId});
  @override
  _ReceiptNotePoFormState createState() => _ReceiptNotePoFormState();
}

class _ReceiptNotePoFormState extends State<ReceiptNotePoForm> {

  Map<String, dynamic> warehouses;
  List<dynamic> warehouseList;

  Map<String, dynamic> vendors;
  List<dynamic> vendorList;

  Map<String, dynamic> customers;
  List<dynamic> customerList;

  final _formKey = GlobalKey<FormState>();
  TextEditingController poController = TextEditingController();
  TextEditingController vesselController = TextEditingController();
  TextEditingController remarkController = TextEditingController();

  getWarehouse() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/warehouse/get-warehouse"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      warehouses = json.decode(response.body);
      warehouseList = warehouses['data'];
    });
  }

  getVendor(client) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/vendor/get-vendor-by-client?client=${client}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      vendors = json.decode(response.body);
      vendorList = vendors['data'];
    });
  }
  getCustomer(client) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/business-party-user/get-business-party-user-by-client?client=${client}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      customers = json.decode(response.body);
      customerList = customers['data'];
    });
  }
submitForm(String ref_no, String vendor, String warehouse,String vessel,String client,String remark) async{
  await http
      .post(Uri.encodeFull("http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/receipt-note/create-po"),
      headers: {

        'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
      },
      body:{
        "GR_Details": {
          "business_party_trx_no": widget.clientId,
          "ref_no": ref_no,
          "vendor_trx_no": vendor,
          "warehouse_trx_no": warehouse,
          "is_cross_docking": "1",
          "vessel_voyage": vessel,
          "business_party_user_trx_no": client,
          "remark": remark
        },
        "Product_List": [
          {
            "product_trx_no": "36",
            "qty": "12",
            "currency":"SGD",
            "buying_price": "100",
            "selling_price":"50"
          },
          {
            "product_trx_no": "37",
            "qty": "13",
            "currency":"SGD",
            "buying_price": "100",
            "selling_price":"50"
          }

        ]

      }

  );
}
//  getData(){
//    print('here');
//  }
  @override
  void initState() {
    // TODO: implement initState
    this.getWarehouse();
    this.getVendor(33);
    this.getCustomer(33);

    super.initState();
  }




  Column FormField(String text, TextEditingController controller) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 12.0),
          child: Text(text),
        ),
        TextField(
          controller: controller,
        )
      ],
    );
  }

String _warehouseValue;
  String _vendorValue;
  String _customerValue;
  @override
  Widget build(BuildContext context) {
    print(warehouseList.length);
    print(vendorList);
    return  Scaffold(
      appBar: AppBar(title: Text('Create PO Form'),),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              FormField("PO No.*",poController),
              Container(
                  padding: EdgeInsets.all(20.0),
                  width: double.infinity,
                  child: DropdownButton(
                    value: _vendorValue,
                    items: vendorList.map((vendor) {
                      return

                        DropdownMenuItem(child: Text(vendor['name']), value: vendor['id'],);

                    }).toList(),
                    onChanged: (value){
                      setState((){
                        _vendorValue = value;
                      });
                    },
                  )
              ),
              Container(
                  padding: EdgeInsets.all(20.0),
                  width: double.infinity,
                  child: DropdownButton(
                    value: _warehouseValue,
                    items: warehouseList.map((warehouse) {
                      return

                        DropdownMenuItem(child: Text(warehouse['warehouse_code']), value: warehouse['trx_no'],);

                    }).toList(),
                    onChanged: (value){
                      setState((){
                        _warehouseValue = value;
                      });
                    },
                  )
              ),
              FormField("Vessel Voyage",vesselController),
              Container(
                  padding: EdgeInsets.all(20.0),
                  width: double.infinity,
                  child: DropdownButton(
                    value: _customerValue,
                    items: customerList.map((customer) {
                      return

                         DropdownMenuItem(child: FittedBox(child: Text(customer['business_party_user_name'])), value: customer['trx_no'],);

                    }).toList(),
                    onChanged: (value){
                      setState((){
                        _customerValue = value;
                      });
                    },
                  )
              ),
              FormField("Remark",remarkController),


              RaisedButton(child: Text('Submit',style: TextStyle(color: Colors.white),),onPressed: (){
                submitForm(vesselController.text, _vendorValue, _warehouseValue,vesselController.text,_customerValue,remarkController.text);
              },color: Colors.blue,)
            ],
          ),
        ),
      ),
    );
  }
}

class ListItem {
  int value;
  String name;

  ListItem(this.value, this.name);
}
