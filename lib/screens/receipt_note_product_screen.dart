import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import '../widgets/badge.dart';
import 'package:provider/provider.dart';
import '../providers/cart.dart';
import 'receipt_cart_screen.dart';

class ReceiptNoteProductsScreen extends StatefulWidget {
  final String clientId;
  ReceiptNoteProductsScreen({this.clientId});

  @override
  _ReceiptNoteProductsScreenState createState() =>
      _ReceiptNoteProductsScreenState();
}

class _ReceiptNoteProductsScreenState extends State<ReceiptNoteProductsScreen> {
  Map<String, dynamic> products;
  List<dynamic> productList;

  getData(clientId) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/product/get-product-by-client?client=${clientId}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      products = json.decode(response.body);
      productList = products['data'];
    });
  }

//  getData(){
//    print('here');
//  }
  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.clientId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final carts = Provider.of<Cart>(context).poItems;
    return Scaffold(
      appBar: AppBar(title: Text('Products'), actions: <Widget>[
        Consumer<Cart>(
          builder: (_, cart, ch) => Badge(
            child: ch,
            value: cart.poItemCount.toString(),
          ),
          child: IconButton(
            icon: Icon(Icons.shopping_cart),
            color: Colors.white,
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => ReceiptCartScreen(cartList: carts, clientId: widget.clientId)));
            },
          ),
        ),
      ]),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: GridView.builder(
          itemBuilder: (BuildContext context, int index) {
            return ProductGridItem(
                productList[index]['trx_no'],
                productList[index]['product_code'],
                productList[index]['description']);
          },
          itemCount: productList == null ? 0 : productList.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 3 / 2,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
          ),
        ),
      ),
    );
  }
}

class ProductGridItem extends StatelessWidget {
  final String productId;
  final String productCode;
  final String productDescription;
  ProductGridItem(this.productId, this.productCode, this.productDescription);
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Cart>(context, listen:false);
    return GestureDetector(
      onTap: () => print('product added'),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: GridTile(
          child: Image(
            image: AssetImage('assets/images/product_default.jpg'),
            fit: BoxFit.cover,
          ),
          footer: GridTileBar(
            backgroundColor: Colors.black87,
            title: FittedBox(
              child: Text(
                productCode,
              ),
            ),
            subtitle: FittedBox(
              child: Text(
                productDescription,

              ),
            ),
            trailing: IconButton(
              icon: Icon(Icons.add),
              onPressed: () {
               product.addPoItem(productId);
              },
            ),
          ),
        ),
      ),
    );
  }
}
