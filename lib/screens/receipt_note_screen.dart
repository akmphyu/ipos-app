import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import 'package:http/http.dart' as http;
import '../widgets/receipt_note_item.dart';
import 'dart:convert';
import 'receipt_note_select_form.dart';

class ReceiptNoteScreen extends StatefulWidget {
  static const routeName = '/receipt_note';

  @override
  _ReceiptNoteScreenState createState() => _ReceiptNoteScreenState();
}

class _ReceiptNoteScreenState extends State<ReceiptNoteScreen> {
  Map<String, dynamic> receipt_note;
  List<dynamic> receiptNoteList;

  String _selectedClient;
  getData() async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/receipt-note/get-po-list?business_party_code=WMO&dateFrom=2020-01-13&dateTo=2020-11-02&jobStatus=pending"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      receipt_note = json.decode(response.body);
      receiptNoteList = receipt_note['msg'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PO"),
      ),
      drawer: Drawer(
        child: MainDrawer(),
      ),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return ReceiptNoteItem(
            receiptId: receiptNoteList[index]['trx_no'],
            ref_no: receiptNoteList[index]['ref_no'],
            order_date: receiptNoteList[index]['order_date'],
            status: receiptNoteList[index]['status'],
            client: receiptNoteList[index]['business_party_code'],
            remark: receiptNoteList[index]['JobRemarks'],
            vendor: receiptNoteList[index]['vendor'],
            warehouse: receiptNoteList[index]['warehouse'],
          );
        },
        itemCount: receiptNoteList == null ? 0 : receiptNoteList.length,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (_) => ReceiptNoteSelectForm() ));
        },
      ),
    );
  }
}
