import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'receipt_note_product_screen.dart';


class ReceiptNoteSelectForm extends StatefulWidget {
  @override
  _ReceiptNoteSelectFormState createState() => _ReceiptNoteSelectFormState();
}

class _ReceiptNoteSelectFormState extends State<ReceiptNoteSelectForm> {
  String _selectedClient;



  List clients = [
    {
      'name': 'WMO',
      'id' : '33'
    },
    {
      'name':'AGL',
      'id': '34'
    },
    {
      'name':'AF',
      'id': '37'
    },
{
'name':'WMO2',
'id': '104'
}
  ];



  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('PO Form'),),
      body: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            DropdownButton(
              value: _selectedClient,
              items: clients.map((client) {
                return DropdownMenuItem(
                    value: client['id'], child: Text(client['name']));
              }).toList(),
              onChanged: (newValue) {
                setState(() {
                  _selectedClient = newValue;
                });
              },
            ),


            RaisedButton(onPressed:(){
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => ReceiptNoteProductsScreen(clientId: _selectedClient,) ));
            },child: Text('Next'),)
          ],
        ),
      ),

    );
  }
}
