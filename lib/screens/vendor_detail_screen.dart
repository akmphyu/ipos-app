import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class VendorDetailScreen extends StatefulWidget {
  static const routeName = '/vendor-detail';
  final String vendorId;
  VendorDetailScreen({this.vendorId});

  @override
  _VendorDetailScreenState createState() => _VendorDetailScreenState();
}

class _VendorDetailScreenState extends State<VendorDetailScreen> {
  Map<String, dynamic> vendors;
  List<dynamic> vendorList;

  getData(id) async {
    http.Response response = await http.get(
        Uri.encodeFull(
            "http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/vendor/get-vendor-by-name?id=${id}"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        });
    this.setState(() {
      vendors = json.decode(response.body);
      vendorList = vendors['data'];
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData(widget.vendorId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${vendorList[0]['name']}"),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(5),
        child: Card(
          elevation: 5,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 10, bottom: 10),
                alignment: Alignment.center,
                child: CircleAvatar(
                  radius: 30.0,
                  child: Image.asset(
                    'assets/images/avatar2.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              VendorText(
                  'Customer', '${vendorList[0]['business_party_abbr_name']}'),
              VendorText(
                  'About', 'Web Designer / UX / Graphic Artist / Coffee Lover'),
              VendorText('Address',
                  '${vendorList[0]['address']},${vendorList[0]['postal_code']},${vendorList[0]['country']}'),
              VendorText('Phone', '${vendorList[0]['phone_numer']}'),
              VendorText(
                  'Contact Person', '${vendorList[0]['contact_person']}'),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RaisedButton(
                      child: Text('Edit'),
                      color: Colors.yellow,

                      onPressed: (){

                      },
                    ),
                    RaisedButton(
                      child: Text('Delete'),
                      color: Colors.red,

                      onPressed: (){

                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class VendorText extends StatelessWidget {
  final String displayName;
  final String displayText;
  VendorText(this.displayName, this.displayText);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              '${displayName}:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            width: 5,
          ),
          Expanded(child: Text(displayText))
        ],
      ),
    );
  }
}
