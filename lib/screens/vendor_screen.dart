import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';
import 'create_vendor.dart';
import '../widgets/client.dart';
import '../widgets/search.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class VendorScreen extends StatefulWidget {
  static const routeName = '/vendor';

  @override
  _VendorScreenState createState() => _VendorScreenState();
}

class _VendorScreenState extends State<VendorScreen> {
  TextEditingController searchController = TextEditingController();
  Map<String, dynamic> vendors;
  List<dynamic> vendorList;

  getData() async{
    http.Response response = await http
        .get(Uri.encodeFull("http://uat.ipos.winspecgroup.com/iPOS_uat/web/v2/vendor/get-vendor"),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': 'Bearer kjljlhjfdfghhjjhghjg'
        }

    );
    this.setState((){
      vendors = json.decode(response.body);
      vendorList = vendors['data'];
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    this.getData();
    super.initState();
  }

  createVendor(BuildContext context) {
    Navigator.of(context).pushNamed(CreateVendor.routeName);
  }

  handleSearch(){

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Vendor"),
      ),
      drawer: Drawer(
        child: MainDrawer(),
      ),
      body: ListView.builder(
      itemBuilder: (BuildContext context, int index){
        return Client(id: vendorList[index]['id'],clientName: vendorList[index]['name'],isVendor: true);
      },
        itemCount: vendorList==null ? 0: vendorList.length,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      floatingActionButton: FloatingActionButton(
        onPressed: () => createVendor(context),
        child: Icon(Icons.add),
      ),
    );
  }
}
