import 'package:flutter/material.dart';
import '../screens/vendor_detail_screen.dart';
import '../screens/client_detail_screen.dart';
class Client extends StatelessWidget {
  final String id;
  final String clientName;
  final bool isVendor;
  Client({this.id,this.clientName, this.isVendor = false});
  @override
  Widget build(BuildContext context) {
    print(this.id);
    print(this.clientName);
    return GestureDetector(
      onTap: () => isVendor ? Navigator.of(context).push(MaterialPageRoute(builder: (_) => VendorDetailScreen(vendorId: id))) : Navigator.of(context).push(MaterialPageRoute(builder: (_) => ClientDetailScreen(clientId:id))),
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Container(
          color: Colors.white54,
          child: ListTile(
            leading: CircleAvatar(backgroundImage:  AssetImage('assets/images/avatar2.png'),),
            title: Text(clientName, style: TextStyle(fontWeight: FontWeight.bold),),
            subtitle: isVendor ? Text("Address"): null,
            trailing: Icon(Icons.edit, color: Colors.orange,),
          ),
        ),
      ),
    );
  }
}
