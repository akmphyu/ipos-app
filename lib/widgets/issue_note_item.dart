import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../screens/issue_note_detail_screen.dart';

class IssueNoteItem extends StatelessWidget {
  final String issue_note_id;
final String ref_no;
final String order_date;
final String delivery_date;
final String remark;
final String is_local;
final String status;
final String client;
IssueNoteItem({this.issue_note_id,this.ref_no,this.order_date,this.client, this.delivery_date,this.remark,this.is_local,this.status});

  @override
  Widget build(BuildContext context) {
    print(issue_note_id);
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (_) => IssueNoteDetailScreen(issue_note_id: issue_note_id)));
      },
      child: Card(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Ref No:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${ref_no}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Client:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${client}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Order Date:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${order_date}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Delivery Date:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${delivery_date}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Local/Vessel:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${is_local == 1 ? "Local" : "Vessel"} '),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Status:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${status}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Remark:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${remark}'),
                      ],
                    ),

                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
