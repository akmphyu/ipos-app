import 'package:flutter/material.dart';
import '../screens/issue_note_screen.dart';
import '../screens/receipt_note_screen.dart';
import '../screens/product_screen.dart';
import '../screens/vendor_screen.dart';
import '../screens/business_party_user_screen.dart';
import '../screens/issue_note_tabs_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title,IconData icon, Function tapHandler){
    return ListTile(
      leading: Icon(icon,size: 26,),
      title: Text(title,style: TextStyle(fontWeight: FontWeight.bold, fontFamily: 'Raleway',fontSize: 20),),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Colors.white,
            child: Image(image: AssetImage('assets/images/Winspec-logo.png'),),
          ),
          SizedBox(height: 20,),
//          buildListTile('Dashboard', Icons.dashboard ,(){
//            Navigator.of(context).pushReplacementNamed('/');
//          }),
          buildListTile('Buy', Icons.calendar_today,(){
            Navigator.of(context).pushReplacementNamed(ReceiptNoteScreen.routeName);
          }),
          buildListTile('Sell', Icons.calendar_today,(){
            Navigator.of(context).pushReplacementNamed(IssueNoteTabsScreen.routeName);
          }),
          buildListTile('Product', Icons.email,(){
            Navigator.of(context).pushReplacementNamed(ProductScreen.routeName);
          }),
          buildListTile('Vendor', Icons.portrait,(){
            Navigator.of(context).pushReplacementNamed(VendorScreen.routeName);
          }),
          buildListTile('Client & Address', Icons.portrait,(){
            Navigator.of(context).pushReplacementNamed(BusinessPartyUserScreen.routeName);
          }),
        ],
      ),
    );
  }
}
