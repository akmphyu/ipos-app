import 'package:flutter/material.dart';
import '../screens/products_screen.dart';

class ProductItem extends StatelessWidget {
  final String id;
  final String title;
  final Color color;

  ProductItem(this.id, this.title, this.color);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      child: GestureDetector(
        onTap: () => Navigator.of(context).pushNamed(ProductsScreen.routeName, arguments: {'title':title}),
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Text(
            title,
            style: Theme.of(context).textTheme.title,
          ),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [color.withOpacity(0.7), color],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight),
              borderRadius: BorderRadius.circular(15)),
        ),
      ),
    );
  }
}
