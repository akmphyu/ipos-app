import 'package:flutter/material.dart';

class ProductsItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(itemBuilder: (ctx, index) {
      return Card(
        margin: EdgeInsets.symmetric(
          vertical: 8,
          horizontal: 5,
        ),
        child: ListTile(
          leading: CircleAvatar(
            radius: 30,
            child: Padding(
              padding: EdgeInsets.all(6),
              child: FittedBox(
                child: Text('image'),
              ),
            ),
          ),
          title: Text(
            'PO Ref No',
            style: Theme.of(context).textTheme.title,
          ),
          subtitle: Column(
            children: <Widget>[
              Text('Order Date'),
              Text('Local or Vessel'),
              Row(
                children: <Widget>[

                ],
              )
            ],
          ),
        ),
      );
    });
  }
}
