import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../screens/receipt_note_detail_screen.dart';

class ReceiptNoteItem extends StatelessWidget {
  final String receiptId;
  final String ref_no;
  final String order_date;
  final String remark;
  final String vendor;
  final String warehouse;
  final String status;
  final String client;
  ReceiptNoteItem({this.receiptId, this.ref_no,this.order_date,this.client, this.vendor,this.remark,this.warehouse,this.status});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (_) => ReceiptNoteDetailScreen(receipt_note_id: receiptId,)));
//      print(receiptId);
      },
      child: Card(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(bottom: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    
                      children: <Widget>[
                        Text('Ref No:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${ref_no}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,

                      children: <Widget>[
                        Text('Client:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${client}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Order Date:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${order_date}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Vendor:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${vendor}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Warehouse:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${warehouse} '),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Status:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${status}'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text('Remark:', style: TextStyle(fontWeight: FontWeight.bold),),
                        Text('${remark}'),
                      ],
                    ),

                  ],
                ),
              ),


            ],
          ),
        ),
      ),
    );
  }
}
